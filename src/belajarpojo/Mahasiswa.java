/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package belajarpojo;

/**
 *
 * @author IndraKus
 */
public class Mahasiswa {
    private String nama;
    private String alamat;
    private String jk;
    private int nohp;
    
    public int getNohp() {
        return nohp;
    }

    public void setNohp(int nohp) {
        this.nohp = nohp;
    }
    
    public String getNama() {
        return nama;
    }

    public String getAlamat() {
        return alamat;
    }

    public String getJk() {
        return jk;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public void setJk(String jk) {
        this.jk = jk;
    }
}
