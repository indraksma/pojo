/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package belajarpojo;
import java.util.Scanner;
import pojo2.MataKuliah;
/**
 *
 * @author IndraKus
 */
public class BelajarPOJO {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Mahasiswa pojo = new Mahasiswa();
        MataKuliah makul = new MataKuliah();
        Scanner inp = new Scanner(System.in);
        String nama,alamat,jk,mk,dosen;
        int nohp,jm,js;
        
        System.out.print("Nama Anda : ");
        nama = inp.nextLine();
        pojo.setNama(nama);
        System.out.print("Alamat Anda : ");
        alamat = inp.nextLine();
        pojo.setAlamat(alamat);
        System.out.print("JK Anda : ");
        jk = inp.nextLine();
        pojo.setJk(jk);
        System.out.print("No HP Anda : ");
        nohp = Integer.parseInt(inp.nextLine());
        pojo.setNohp(nohp);        
        System.out.println("---------------------------");
        System.out.print("Mata Kuliah : ");
        mk = inp.nextLine();
        makul.setNama_mk(mk);
        System.out.print("Nama Dosen : ");
        dosen = inp.nextLine();
        makul.setDosen(dosen);
        System.out.print("Jam Masuk : ");
        jm = inp.nextInt();
        makul.setJammulai(jm);
        System.out.print("Jam Selesai : ");
        js = inp.nextInt();
        makul.setJamselesai(js);
        System.out.println("--------Output-------");
        System.out.println("Nama : "+pojo.getNama());
        System.out.println("Alamat : "+pojo.getAlamat());
        System.out.println("JK : "+pojo.getJk());
        System.out.println("No HP : "+pojo.getNohp());
        System.out.println("---------------------------");
        System.out.println("Mata Kuliah : "+makul.getNama_mk());
        System.out.println("Nama Dosen : "+makul.getDosen());
        System.out.println("Jam Mulai : "+makul.getJammulai());
        System.out.println("Jam Mulai : "+makul.getJamselesai());
        System.out.println("---------------------------");
        
    }
    
}
