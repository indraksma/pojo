/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pojo2;

/**
 *
 * @author IndraKus
 */
public class MataKuliah {
    private String nama_mk;
    private String dosen;
    private int jammulai;
    private int jamselesai;

    public String getNama_mk() {
        return nama_mk;
    }

    public void setNama_mk(String nama_mk) {
        this.nama_mk = nama_mk;
    }

    public String getDosen() {
        return dosen;
    }

    public void setDosen(String dosen) {
        this.dosen = dosen;
    }

    public int getJammulai() {
        return jammulai;
    }

    public void setJammulai(int jammulai) {
        this.jammulai = jammulai;
    }

    public int getJamselesai() {
        return jamselesai;
    }

    public void setJamselesai(int jamselesai) {
        this.jamselesai = jamselesai;
    }
    
}
